#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void round10(const char *n)
{
    int len = strlen(n);
    bool isRoundUp = false;
    int i = len - 1;
    bool plusOneNumber = false;
    
    if (*(n + len - 1) >= '5')
        isRoundUp = true;

    if (isRoundUp == true)
    {
        while(true)
        {
            if (i == 0) 
            {
                if (*n == '9' || len == 1) 
                {   
                    plusOneNumber = true;
                    printf("1");
                }
                break;
            }
            if(i == len - 1)
            {
                --i;
                continue;
            }
            if (*(n + i) == '9')
                --i;         
            else 
                break;
        }
    }
    
    for (int a = 0; a < len - 1; a++)    
    {
        if (isRoundUp == true && plusOneNumber == false)
        {
            if (a == i)
            {    
                printf("%d", *(n+a) - '0' + 1);
                continue;
            }
        }
        
        if (isRoundUp == true)
        {
            if(a >= i)
            {
                printf("0");
                continue;
            }
        }   
        printf("%d", *(n+a) - '0');
    }
    printf("0\n");

}

int main() 
{
    round10("1234");  // 1230
    round10("199");   // 200
    round10("17");    // 20
    round10("791");   // 790
    round10("2");     // 0
    round10("0");     // 0
    round10("999992"); // 999990
    round10("999998"); // 1000000
    round10("012345"); // 012350
    round10("5");      // 10
    return 0;
}
