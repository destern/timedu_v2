#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void reverse(const char *s)
{
    int strLen = strlen(s);
    int wordEndIndex = 0;
    int wordStartIndex = 0;
    bool havePoint = false;
    
    if (s == NULL)
        return;
    
    for (int i = 0; i < strLen; ++i)
        if (s[i] == '.')
        {
            havePoint = true;
            break;
        }
        
    wordStartIndex = strLen - 1;
    if (!havePoint)
    {
        printf("%s", s);
        printf("\n");
        return;
    }

    for (int i = strLen; i >= 0; --i)
    {
        if (s[i - 1] == '.' || i - 1 < 0)
        {
            wordEndIndex = i;
            for (int a = wordEndIndex; a <= wordStartIndex; ++a) 
            {
                printf("%c", s[a]);
            }
            wordStartIndex = wordEndIndex - 2; // Доходим до следующего конца слова
            if (i - 1 >= 0)
                printf(".");
        }          
    }

    printf("\n");
}

int main() 
{
    reverse("hello.world");      // world.hello
    reverse("where.is.my.mind"); // mind.my.is.where
    reverse("abc.def.ghi.jkl.mno.pqr.stu.vwx.yz"); // yz.vwx.stu.pqr.mno.jkl.ghi.def.abc
    reverse("mind.my.is.where"); // where.is.my.mind
    reverse(""); // 
    reverse("hi"); // hi
    return 0;
}