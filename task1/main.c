#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int prosoft_atoi(const char *str)
{
    int result = 0;
    if(str == NULL)
        return 0;
    int strSize = strlen(str);
    bool isNegative = false;
    int cycleStartNum = 0;
    if(str[0] == '-')
    {
        cycleStartNum = 1;
        isNegative = true;
    }
    for (int i = cycleStartNum; i < strSize ; ++i)
    {
        if (str[cycleStartNum] >= '0' && str[cycleStartNum] <= '9')
        {
            result = result * 10 + (str[cycleStartNum] - '0');
            ++str;
        }
        else
            break;
        }
        return (isNegative) ? -result : result;
}

int main() 
{
    printf("Test1: %d\n", prosoft_atoi("1"));         // 1
    printf("Test2: %d\n", prosoft_atoi("-1"));        // -1
    printf("Test3: %d\n", prosoft_atoi("1234567890"));// 1234567890
    printf("Test4: %d\n", prosoft_atoi(""));          // 0
    printf("Test5: %d\n", prosoft_atoi("test22"));    // 0
    printf("Test6: %d\n", prosoft_atoi("22test"));    // 22
    printf("Test7: %d\n", prosoft_atoi(NULL));        // 0
    return 0;
}