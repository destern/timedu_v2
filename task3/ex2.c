#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void add_bin(const char *s1, const char *s2)
{
    int maxLen = (strlen(s1) >= strlen(s2)) ? strlen(s1) : strlen(s2);
    char sum [maxLen + 1];
    int transferNext = 0;   
    bool isFirstStrLengthReached = false;
    bool isSecondStrLengthReached = false;
    int s1Iter = strlen(s1) - 1;
    int s2Iter = strlen(s2) - 1;
    int sumIter = 0;   
    while (!isFirstStrLengthReached || !isSecondStrLengthReached)
    {
        if (s1Iter == -1)
            isFirstStrLengthReached = true;
        
        if (s2Iter == -1)
            isSecondStrLengthReached = true;
        
        if (!isFirstStrLengthReached && !isSecondStrLengthReached)
        {
            // 2 - двойное вычитание '0' на 2 символа
            int i = (*(s1+s1Iter) + *(s2+s2Iter) - (2 * '0')) + transferNext;
            if (i >= 2)
            {
                i -= 2;
                transferNext = 1;
            }
            else
                transferNext = 0;
            
            *(sum + sumIter) = i + '0';
            --s1Iter;
            --s2Iter;
            ++sumIter;
        }
        if (!isFirstStrLengthReached && isSecondStrLengthReached) 
        {
            int i = *(s1+s1Iter) - '0' + transferNext;
            if (i == 2) 
            {
                transferNext = 1;
                i -= 2;
            }
            else
                transferNext = 0;
            
            *(sum + sumIter) = i + '0';
            --s1Iter;
            ++sumIter;
        }
        if (isFirstStrLengthReached && !isSecondStrLengthReached) 
        {
            int i = *(s2+s2Iter) - '0' + transferNext;
            
            if (i == 2) {
                transferNext = 1;
                i -= 2;
            }
            else
                transferNext = 0;
            
            *(sum + sumIter) = i + '0';
            --s2Iter;
            ++sumIter;
        }
    }
    
    if (transferNext == 1)
        *(sum + sumIter++) = '1';
    
    bool outputCheck = false;     
    bool removeNull = false;
    for (int i = sumIter - 1; i >= 0; --i)
    {
        if (*(sum + i) == '1')
            removeNull = true;
        
        if (!removeNull)
            continue;
        
        printf("%d", *(sum + i) - '0');
        outputCheck = true;
    }
    if (!outputCheck)
        printf("0");
    
    printf("\n");
}

int main()
{
    add_bin("1",   "10");// 11
    add_bin("1",   "1"); // 10
    add_bin("1111","1"); // 10000
    add_bin("10", "10"); // 100
    add_bin("0", "0"); // 0
    add_bin("110", "10"); // 1000
    return 0;
}
